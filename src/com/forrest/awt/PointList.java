package com.forrest.awt;

/** PointList.java defines a data structure to store (x,y) points.
 *  Attributes: separate int arrays for x and y coordinates;
 *              number of points
 *  Methods:    Default constructor; add a point to end of the list;
 *              remove a point from the end of the list; retrieve
 *              each coordinate of point at end / front of the the list;
 *              accessors
 */

import java.awt.Point;

class PointList extends Object {
	/**
	 * PointList constructor Postcondition: myX and myY are int arrays with
	 * capacity DEFAULT_SIXE; myPointCount == 0.
	 */
	public PointList() {
		myX = new int[DEFAULT_SIZE];
		myY = new int[DEFAULT_SIZE];
		myPointCount = 0;
	}

	// --- Push operations ---
	/*
	 * Push a Point Receive: Point p Postcondition: Coordinates of p have been
	 * added to myX and myY arrays.
	 */
	public void pushPoint(Point p) {
		pushPoint(p.x, p.y);
	}

	/*
	 * Push a point with coordinates x and y Receive: ints x and y
	 * Postcondition: x has been added to array myX, y to array myY.
	 */
	public void pushPoint(int x, int y) {
		if (myPointCount >= myX.length) // arrays are full
		{
			int newCapacity = myX.length * 2; // create bigger ones
			int[] tempX = new int[newCapacity];
			int[] tempY = new int[newCapacity];

			for (int i = 0; i < myPointCount; i++) // put my values in them
			{
				tempX[i] = myX[i];
				tempY[i] = myY[i];
			}

			myX = tempX; // replace old arrays
			myY = tempY; // with them
		}

		myX[myPointCount] = x;
		myY[myPointCount] = y;
		myPointCount++;
	}

	/*
	 * Pop operation Postcondition: Point at end of Pointlist (last one added)
	 * has been removed.
	 */
	public Point popPoint() {
		Point result = null;
		if (myPointCount > 0) {
			myPointCount--;
			result = new Point(myX[myPointCount], myY[myPointCount]);
		}
		return result;
	}

	// -- Retrieve coordinate of last node
	/*
	 * Get x coordinate of last node Return: int at the end of array myX or -1
	 * if there is none
	 */
	public int lastX() {
		if (myPointCount > 0)
			return myX[myPointCount - 1];
		else
			return -1;
	}

	/*
	 * Get y coordinate of last node Return: int at the end of array myY or -1
	 * if there is none
	 */
	public int lastY() {
		if (myPointCount > 0)
			return myY[myPointCount - 1];
		else
			return -1;
	}

	// -- Retrieve coordinate of first node
	/*
	 * Get x coordinate of first node Return: int at the beginning of array myX
	 * or -1 if there is none
	 */
	public int firstX() {
		if (myPointCount > 0)
			return myX[0];
		else
			return -1;
	}

	/*
	 * Get y coordinate of first node Return: int at the beginning of array myY
	 * or -1 if there is none
	 */
	public int firstY() {
		if (myPointCount > 0)
			return myY[0];
		else
			return -1;
	}

	// --- Accessors ---
	/*
	 * X coordinates Return: myX
	 */
	public int[] xCoordinates() {
		return myX;
	}

	/*
	 * Y coordinates Return: myY
	 */
	public int[] yCoordinates() {
		return myY;
	}

	/*
	 * Number of points Return: myPointCount
	 */
	public int pointCount() {
		return myPointCount;
	}

	/*
	 * Clear the polygon Postcondition: Point list is empty.
	 */
	public void clear() {
		myPointCount = 0;
	}

	/*
	 * String converter -- for output purposes Return: String representation of
	 * polygon
	 */
	public String toString() {
		String result = "";
		for (int i = 0; i < myPointCount; i++)
			result += " (" + myX[i] + "," + myY[i] + ")";

		return result;
	}

	// --- Attribute constant and variables ---
	private final static int DEFAULT_SIZE = 8;

	private int[] myX;
	private int[] myY;
	private int myPointCount;
}
