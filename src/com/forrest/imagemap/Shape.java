package com.forrest.imagemap;

import java.awt.Point;
import java.util.Arrays; // toString
import java.awt.Polygon;
import java.awt.Rectangle;
import com.forrest.imagemap.Circle;

public class Shape implements java.io.Serializable {
	public static String TYPE_RECT = "rect";
	public static String TYPE_CIRCLE = "circle";
	public static String TYPE_POLY = "poly";

	String type;
	Circle circle;
	Rectangle rect;
	Polygon poly;

	public Shape() {
		super();
	}
	/**
	 * Constructor for rectangle objects
	 */
	public Shape(int x, int y, int x2, int y2) {
		this.type = TYPE_RECT;
		this.rect = new Rectangle();
		setRect(x, y, x2, y2);
	}
	public Shape(Rectangle r) {
		this.type = TYPE_RECT;
		this.rect = r;
	}

	/**
	 * Constructor for circle objects
	 */
	public Shape(Point m, int r) {
		this.type = TYPE_CIRCLE;
		this.circle = new Circle(new Point(m), r);
	}

	/**
	 * Constructor for polygon objects
	 */
	public Shape(Point first) {
		this.type = TYPE_POLY;
		poly = new Polygon();
		poly.addPoint((int) first.getX(), (int) first.getY());
	}

	public Shape(Circle c) {
		this.type = TYPE_POLY;
		circle = c;
	}

	public Shape(Polygon p) {
		this.type = TYPE_POLY;
		poly = p;
	}
	
	@Override
	public String toString() {
		String rval = "";
		
		if(type.equalsIgnoreCase(TYPE_RECT)) {
			rval = "Rectangle(x=" + rect.x + 
						   ", y=" + rect.y + 
						   ", width=" + rect.width + 
						   ", height=" + rect.height + ")";
		}
		else if(type.equalsIgnoreCase(TYPE_POLY)) {
			rval = "Polygon(xpoints=" + Arrays.toString(poly.xpoints) + 
						 ", ypoints=" + Arrays.toString(poly.ypoints) + 
						 ", npoints=" + poly.npoints + ")";
		}
		else if (type.equalsIgnoreCase(TYPE_CIRCLE)) {
			rval = "Circle(x=" + circle.getCenterPoint().x +
						", y=" + circle.getCenterPoint().y +
						", radius=" + circle.getRadius() + ")";
		}
		return rval;
	}
	/**
	 * @return the circle
	 */
	public Circle getCircle() {
		return circle;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the rect
	 */
	public Rectangle getRect() {
		return rect;
	}
	/**
	 * @param rect the rect to set
	 */
	public void setRect(Rectangle rect) {
		this.rect = rect;
	}
	public void setRect(int x1, int y1, int x2, int y2) {
		int x, y, w, h;
		if (x2 > x1) {
			x = x1;
			w = x2 - x1;
		} else {
			x = x2;
			w = x1 - x2;
		}

		if (y2 > y1) {
			y = y1;
			h = y2 - y1;
		} else {
			y = y2;
			h = y1 - y2;
		}

		this.rect = new Rectangle(x, y, w, h);
	}
	/**
	 * @return the poly
	 */
	public Polygon getPoly() {
		return poly;
	}
	/**
	 * @param poly the poly to set
	 */
	public void setPoly(Polygon poly) {
		this.poly = poly;
	}
	public boolean isRectangle() { 
		return type.equalsIgnoreCase(TYPE_RECT);
	}
	public boolean isCircle() { 
		return type.equalsIgnoreCase(TYPE_CIRCLE);
	}
	public boolean isPolygon() { 
		return type.equalsIgnoreCase(TYPE_POLY);
	}
}
