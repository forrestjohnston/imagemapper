package com.forrest.imagemap;

/*
 ImageMap Client-side imagemap creator
 Copyright (C) 2001-2003  Andreas Tetzl
 www.tetzl.de
 andreas@tetzl.de

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;

import javax.imageio.ImageIO;
import javax.swing.*;

import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.border.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.JOptionPane;




// 3rd Party, non-JavaSDK Packages
import com.google.gson.Gson;

/**
 * Original code from the following:
 * Title:       ImageMap
 * Description:	client side imagemap creator
 * Copyright:	Copyright (c) 2001
 * Company:		webdesign-pirna.de
 * @author		Andreas Tetzl
 * @version 1.0
 *
 * MAJOR changes by Forrest Johnston 2013-2014
 *   to create ImageMapEditor prototype.
 */

public class Imagemapper extends JFrame implements ActionListener {
	String DEFAULT_MAP_NAME = "Map";
	ImageMap imageMap = new ImageMap(-1, DEFAULT_MAP_NAME, null,
									 new Dimension(0, 0));
	// String imageSrc = "";
	// String mapName = "";
	String directory = ".";
	String image_dir = ".";
	File image_file = null;
	Image image;
	boolean mapChanged = false;
	String appURL = "http://localhost:8000/devicemaps/";
	String jsonSaveDir = "D:\\Projects\\workspace\\ImageMapper\\sampleDevice\\json\\"; // DEBUG

	ShapeList shapeList = new ShapeList();
	ImagemapShape selectedShape = null;
	ImagemapPanel imagemapPanel;
	ImageAreaEdit areaDlg;

	BufferedImage emptyImage = new BufferedImage(600, 500,
			BufferedImage.TYPE_INT_RGB);

	JPopupMenu popup;
	JMenuItem menuItem_title, menuItem_rempoint, menuItem_remobject,
			menuItem_editInfo, menuItem_editalt, menuItem_convert;

	JToolBar jToolBar1 = new JToolBar();
	JButton jButton_rectangle = new JButton();
	JTabbedPane jTabbedPane1 = new JTabbedPane();
	JScrollPane jScrollPane_image;
	JScrollPane jScrollPane_json = new JScrollPane();
	JButton jButton_circle = new JButton();
	JButton jButton_polygon = new JButton();
	JButton jButton_imgmapFile = new JButton();
	JButton jButton_imgsrcPrev = new JButton();
	JButton jButton_imgsrcNext = new JButton();
	JButton jButton_imgsrcFile = new JButton();
	// JButton jButton_imgmapSelect = new JButton();
	// JButton jButton_imgsrcSelect = new JButton();

	JMenuBar jMenuBar1 = new JMenuBar();
	JMenu jMenu_file = new JMenu();
	JMenuItem jMenuItem_new = new JMenuItem();
	JMenuItem jMenuItem_imgsrcFile = new JMenuItem();
	JMenuItem jMenuItem_imgmapFile = new JMenuItem();
	JMenuItem jMenuItem_exit = new JMenuItem();
	JMenu jMenu_help = new JMenu();
	JMenuItem jMenuItem_about = new JMenuItem();
	Component component1;
	Component component2;
	Component component3;
	JButton jButton_about = new JButton();
	JButton jButton_new = new JButton();
	BorderLayout borderLayout1 = new BorderLayout();
	JToolBar jToolBar3 = new JToolBar();
	JLabel statusBar = new JLabel();
	JMenu jMenu1 = new JMenu();
	JCheckBoxMenuItem jCheckBoxMenuItem_snap = new JCheckBoxMenuItem();
	JCheckBoxMenuItem jCheckBoxMenuItem_antialias = new JCheckBoxMenuItem();
	JPanel jPanel_json = new JPanel();
	JButton jButton_savejson = new JButton();
	JButton jButton_postjson = new JButton();
	JButton jButton_copyjson = new JButton();
	JToolBar jToolBar2 = new JToolBar();
	BorderLayout borderLayout3 = new BorderLayout();
	JTextArea jTextArea_json = new JTextArea();
	JButton jButton_addpoint = new JButton();
	JButton jButton_delpoint = new JButton();
	TitledBorder titledBorder1;
	JButton jButton_help = new JButton();
	JMenuItem jMenuItem_help = new JMenuItem();
	JMenu jMenu_zoom = new JMenu();
	JRadioButtonMenuItem jMenuItem_zoom1 = new JRadioButtonMenuItem();
	JRadioButtonMenuItem jMenuItem_zoom2 = new JRadioButtonMenuItem();
	JRadioButtonMenuItem jMenuItem_zoom4 = new JRadioButtonMenuItem();
	JRadioButtonMenuItem jMenuItem_zoom8 = new JRadioButtonMenuItem();
	ButtonGroup zoomButtonGroup = new ButtonGroup();

	Rule hRule = null;
	Rule vRule = null;

	/** Construct */
	public Imagemapper() {
		// Image si = new
		// ImageIcon(Imagemap.class.getResource("images/splash.jpg")).getImage();
		// Splash splash = new Splash(this, si);

		enableEvents(AWTEvent.WINDOW_EVENT_MASK);
		try {
			jbInit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** Component initialization */
	private void jbInit() throws Exception {
		titledBorder1 = new TitledBorder("");
		setIconImage(Toolkit.getDefaultToolkit().createImage(
				Imagemapper.class.getResource("images/frameicon.gif")));

		component1 = Box.createHorizontalStrut(8);
		component2 = Box.createHorizontalStrut(8);
		component3 = Box.createHorizontalStrut(8);

		this.setSize(new Dimension(600, 500));
		this.setTitle("ScreenMapper 1.0");

		Graphics2D g = emptyImage.createGraphics();
		g.setColor(Color.gray);
		g.fillRect(0, 0, 600, 500);
		image = emptyImage;
		imagemapPanel = new ImagemapPanel(image, shapeList, statusBar,
				jCheckBoxMenuItem_snap, jCheckBoxMenuItem_antialias, this);
		jScrollPane_image = new JScrollPane(imagemapPanel);

		jCheckBoxMenuItem_snap.setSelected(true);
		jCheckBoxMenuItem_antialias.setSelected(true);

		jMenu_file.setText("File");
		jMenuItem_new.setText("New");
		jMenuItem_new.setAccelerator(javax.swing.KeyStroke.getKeyStroke(78,
				java.awt.event.KeyEvent.CTRL_MASK, false));
		jMenuItem_new.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jMenuItem_new_actionPerformed(e);
			}
		});
		jMenuItem_imgsrcFile.setText("Open Image File ...");
		jMenuItem_imgsrcFile.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
				79, java.awt.event.KeyEvent.CTRL_MASK, false));
		jMenuItem_imgsrcFile
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(ActionEvent e) {
						jMenuItem_imgsrcFile_actionPerformed(e);
					}
				});
		jMenuItem_imgmapFile.setText("Read map from json ...");
		jMenuItem_imgmapFile.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
				72, java.awt.event.KeyEvent.CTRL_MASK, false));
		jMenuItem_imgmapFile
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(ActionEvent e) {
						jMenuItem_imgmapFile_actionPerformed(e);
					}
				});
		jMenuItem_exit.setText("Exit");
		jMenuItem_exit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(88,
				java.awt.event.KeyEvent.CTRL_MASK, false));
		jMenuItem_exit.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jMenuItem_exit_actionPerformed(e);
			}
		});
		jMenu_help.setText("Help");
		jMenuItem_about.setText("About Imagemap");
		jMenuItem_about.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jMenuItem_about_actionPerformed(e);
			}
		});
		jButton_about.setToolTipText("About ImageMap");
		jButton_about.setIcon(new ImageIcon(Imagemapper.class
				.getResource("images/about.gif")));
		jButton_about.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jButton_about_actionPerformed(e);
			}
		});
		jButton_new.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jButton_new_actionPerformed(e);
			}
		});
		jScrollPane_image.setDoubleBuffered(true);
		jButton_new.setToolTipText("Clear Imagemap and Image");
		jButton_new.setIcon(new ImageIcon(Imagemapper.class
				.getResource("images/new.gif")));
		jButton_new.setMnemonic('N');
		/*
		jButton_imgsrcSelect.setToolTipText("Select Image Source (web)");
		jButton_imgsrcSelect.setIcon(new ImageIcon(Imagemapper.class
				.getResource("images/camera.gif")));
		jButton_imgsrcSelect.setMnemonic('S');
		jButton_imgmapSelect.setToolTipText("Select DeviceMap (web)");
		jButton_imgmapSelect.setIcon(new ImageIcon(Imagemapper.class
				.getResource("images/map.gif")));
		jButton_imgmapSelect.setMnemonic('S');
		*/
		jButton_imgsrcFile.setToolTipText("Open Image File");
		jButton_imgsrcFile.setIcon(new ImageIcon(Imagemapper.class
				.getResource("images/open.gif")));
		jButton_imgsrcFile.setMnemonic('O');
		jButton_imgsrcPrev.setToolTipText("Open Previous Image File");
		jButton_imgsrcPrev.setIcon(new ImageIcon(Imagemapper.class
				.getResource("images/arrowLeft.gif")));
		//jButton_imgsrcPrev.setMnemonic('O');
		jButton_imgsrcNext.setToolTipText("Open Next Image File");
		jButton_imgsrcNext.setIcon(new ImageIcon(Imagemapper.class
				.getResource("images/arrowRight.gif")));
		//jButton_imgsrcNext.setMnemonic('O');
		jButton_imgmapFile.setToolTipText("Open JSON DeviceMap File");
		jButton_imgmapFile.setIcon(new ImageIcon(Imagemapper.class
				.getResource("images/image.gif")));
		jButton_imgmapFile.setMnemonic('J');
		jButton_rectangle.setToolTipText("Draw Rectangular Shape");
		jButton_rectangle.setIcon(new ImageIcon(Imagemapper.class
				.getResource("images/rect.gif")));
		jButton_rectangle.setMnemonic('R');
		jButton_circle.setToolTipText("Draw Circle Shape");
		jButton_circle.setIcon(new ImageIcon(Imagemapper.class
				.getResource("images/circle.gif")));
		jButton_circle.setMnemonic('C');
		jButton_polygon.setToolTipText("Draw Polygon Shape");
		jButton_polygon.setIcon(new ImageIcon(Imagemapper.class
				.getResource("images/poly.gif")));
		jButton_polygon.setMnemonic('P');
		statusBar.setText(" ");
		jMenu1.setText("Options");
		jCheckBoxMenuItem_snap.setText("snap to existing points");
		jCheckBoxMenuItem_antialias.setText("anti alias lines");

		jButton_savejson.setToolTipText("Save JSON Code to File");
		jButton_savejson.setIcon(new ImageIcon(Imagemapper.class
				.getResource("images/save.gif")));
		jButton_savejson.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jButton_savejson_actionPerformed(e);
			}
		});

		jButton_copyjson.setToolTipText("Copy JSON Code to Clipboard");
		jButton_copyjson.setIcon(new ImageIcon(Imagemapper.class
				.getResource("images/copy.gif")));
		jButton_copyjson.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jButton_copyjson_actionPerformed(e);
			}
		});

		jButton_postjson.setToolTipText("Post JSON Code to WebApp");
		jButton_postjson.setIcon(new ImageIcon(Imagemapper.class
				.getResource("images/postit.gif")));
		jButton_postjson.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jButton_postjson_actionPerformed(e);
			}
		});

		jPanel_json.setLayout(borderLayout3);
		jButton_addpoint.setToolTipText("Add Point to Polygon");
		jButton_addpoint.setIcon(new ImageIcon(Imagemapper.class
				.getResource("images/addpoint.gif")));
		jButton_addpoint.setMnemonic('A');
		jButton_addpoint.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jButton_addpoint_actionPerformed(e);
			}
		});
		jButton_delpoint.setToolTipText("Remove Point from Polygon");
		jButton_delpoint.setIcon(new ImageIcon(Imagemapper.class
				.getResource("images/delpoint.gif")));
		jButton_delpoint.setMnemonic('D');
		jButton_delpoint.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jButton_delpoint_actionPerformed(e);
			}
		});
		jButton_help.setIcon(new ImageIcon(Imagemapper.class
				.getResource("images/help.gif")));
		jButton_help.setToolTipText("Help");
		jButton_help.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jButton_help_actionPerformed(e);
			}
		});
		jMenuItem_help.setText("Help");
		jMenuItem_help.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jMenuItem_help_actionPerformed(e);
			}
		});

		jMenu_zoom.setText("Zoom");
		jMenuItem_zoom1.setText("Zoom Factor 1");
		jMenuItem_zoom1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(49,
				java.awt.event.KeyEvent.CTRL_MASK, false));
		jMenuItem_zoom1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jMenuItem_zoom_actionPerformed(e);
			}
		});
		jMenuItem_zoom2.setText("Zoom Factor 2");
		jMenuItem_zoom2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(50,
				java.awt.event.KeyEvent.CTRL_MASK, false));
		jMenuItem_zoom2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jMenuItem_zoom_actionPerformed(e);
			}
		});
		jMenuItem_zoom4.setText("Zoom Factor 4");
		jMenuItem_zoom4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(52,
				java.awt.event.KeyEvent.CTRL_MASK, false));
		jMenuItem_zoom4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jMenuItem_zoom_actionPerformed(e);
			}
		});
		jMenuItem_zoom8.setText("Zoom Factor 8");
		jMenuItem_zoom8.setAccelerator(javax.swing.KeyStroke.getKeyStroke(56,
				java.awt.event.KeyEvent.CTRL_MASK, false));
		jMenuItem_zoom8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jMenuItem_zoom_actionPerformed(e);
			}
		});
		jMenuItem_zoom1.setSelected(true);
		zoomButtonGroup.add(jMenuItem_zoom1);
		zoomButtonGroup.add(jMenuItem_zoom2);
		zoomButtonGroup.add(jMenuItem_zoom4);
		zoomButtonGroup.add(jMenuItem_zoom8);

		jMenuBar1.add(jMenu_file);
		jMenuBar1.add(jMenu1);
		jMenuBar1.add(jMenu_zoom);
		jMenuBar1.add(jMenu_help);
		this.setJMenuBar(jMenuBar1);

		// Create the popup menu.
		popup = new JPopupMenu();
		menuItem_editInfo = new JMenuItem("Edit AreaInfo");
		menuItem_editInfo.addActionListener(this);
		popup.add(menuItem_editInfo);
		popup.addSeparator();
		menuItem_remobject = new JMenuItem("Remove Shape");
		menuItem_remobject.addActionListener(this);
		popup.add(menuItem_remobject);
		menuItem_rempoint = new JMenuItem("Remove Point");
		menuItem_rempoint.addActionListener(this);
		popup.add(menuItem_rempoint);
		/*
		 * menuItem_editalt = new JMenuItem("Edit ALT");
		 * menuItem_editalt.addActionListener(this);
		 * popup.add(menuItem_editalt);
		 */
		menuItem_convert = new JMenuItem("Convert to Polygon");
		menuItem_convert.addActionListener(this);
		popup.add(menuItem_convert);

		jScrollPane_image
				.addComponentListener(new java.awt.event.ComponentAdapter() {
					public void componentShown(ComponentEvent e) {
						jScrollPane_image_componentShown(e);
					}
				});
		jButton_imgmapFile
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(ActionEvent e) {
						jButton_imgmapFile_actionPerformed(e);
					}
				});
		jButton_imgsrcPrev
		.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jButton_imgsrcPrev_actionPerformed(e);
			}
		});
		jButton_imgsrcFile
		.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jButton_imgsrcFile_actionPerformed(e);
			}
		});
		jButton_imgsrcNext
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(ActionEvent e) {
						jButton_imgsrcNext_actionPerformed(e);
					}
				});
		/*
		jButton_imgsrcSelect
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(ActionEvent e) {
						jButton_imgsrcSelect_actionPerformed(e);
					}
				});
		jButton_imgmapSelect
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(ActionEvent e) {
						jButton_imgmapSelect_actionPerformed(e);
					}
				});
		*/
		jScrollPane_image
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		jScrollPane_image
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		hRule = new Rule(Rule.HORIZONTAL, imagemapPanel);
		vRule = new Rule(Rule.VERTICAL, imagemapPanel);
		hRule.setPreferredWidth(10);
		vRule.setPreferredHeight(10);
		jScrollPane_image.setColumnHeaderView(hRule);
		jScrollPane_image.setRowHeaderView(vRule);

		// Add listener to components that can bring up popup menus.
		imagemapPanel.addMouseListener(new PopupListener());

		jButton_rectangle
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(ActionEvent e) {
						jButton_rectangle_actionPerformed(e);
					}
				});
		jPanel_json.addComponentListener(new java.awt.event.ComponentAdapter() {
			public void componentShown(ComponentEvent e) {
				jPanel_json_componentShown(e);
			}
		});
		jButton_circle.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jButton_circle_actionPerformed(e);
			}
		});
		jButton_polygon.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jButton_polygon_actionPerformed(e);
			}
		});

		jPanel_json.add(jToolBar2, BorderLayout.NORTH);
		jPanel_json.add(jScrollPane_json, BorderLayout.CENTER);
		this.getContentPane().add(jTabbedPane1, BorderLayout.CENTER);
		jTabbedPane1.add(jScrollPane_image, "Image");
		jTabbedPane1.add(jPanel_json, "DeviceMap text");
		jScrollPane_json.getViewport().add(jTextArea_json, null);
		jToolBar2.add(jButton_savejson, null);
		jToolBar2.add(jButton_copyjson, null);
		jToolBar2.add(jButton_postjson, null);
		this.getContentPane().add(jToolBar1, BorderLayout.NORTH);
		jToolBar1.add(jButton_new, null);
		// jToolBar1.add(jButton_imgmapSelect, null);
		// jToolBar1.add(jButton_imgsrcSelect, null);
		jToolBar1.add(component1, null);
		jToolBar1.add(jButton_imgsrcPrev, null);
		jToolBar1.add(jButton_imgsrcFile, null);
		jToolBar1.add(jButton_imgsrcNext, null);
		jToolBar1.add(jButton_imgmapFile, null);
		jToolBar1.add(component2, null);
		jToolBar1.add(jButton_rectangle, null);
		jToolBar1.add(jButton_circle, null);
		jToolBar1.add(jButton_polygon, null);
		jToolBar1.add(jButton_addpoint, null);
		jToolBar1.add(jButton_delpoint, null);
		jToolBar1.add(component3, null);
		jToolBar1.add(jButton_about, null);
		jToolBar1.add(jButton_help, null);
		this.getContentPane().add(jToolBar3, BorderLayout.SOUTH);
		jToolBar3.add(statusBar, null);
		jMenu_file.add(jMenuItem_new);
		jMenu_file.add(jMenuItem_imgsrcFile);
		jMenu_file.add(jMenuItem_imgmapFile);
		jMenu_file.addSeparator();
		jMenu_file.add(jMenuItem_exit);
		jMenu_help.add(jMenuItem_about);
		jMenu_help.add(jMenuItem_help);
		jMenu1.add(jCheckBoxMenuItem_snap);
		jMenu1.add(jCheckBoxMenuItem_antialias);
		jMenu_zoom.add(jMenuItem_zoom1);
		jMenu_zoom.add(jMenuItem_zoom2);
		jMenu_zoom.add(jMenuItem_zoom4);
		jMenu_zoom.add(jMenuItem_zoom8);

	}

	// static initializer for setting look & feel
	/*
	 * static { try {
	 * //UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	 * //UIManager
	 * .setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName()); }
	 * catch(Exception e) { } }
	 */

	void jButton_rectangle_actionPerformed(ActionEvent e) {
		imagemapPanel.set_action(ImagemapPanel.ACTION_RECT);
	}

	void jButton_circle_actionPerformed(ActionEvent e) {
		imagemapPanel.set_action(ImagemapPanel.ACTION_CIRCLE);
	}

	void jButton_polygon_actionPerformed(ActionEvent e) {
		imagemapPanel.set_action(ImagemapPanel.ACTION_POLY);
	}

	void jButton_addpoint_actionPerformed(ActionEvent e) {
		imagemapPanel.set_action(ImagemapPanel.ACTION_ADDPOINT);
	}

	void jButton_delpoint_actionPerformed(ActionEvent e) {
		imagemapPanel.set_action(ImagemapPanel.ACTION_DELPOINT);
	}

	void jButton_imgsrcFile_actionPerformed(ActionEvent e) {
		jMenuItem_imgsrcFile_actionPerformed(e);
	}
	void jButton_imgsrcPrev_actionPerformed(ActionEvent e) {
		jMenuItem_imgsrcPrev_actionPerformed(e);
	}
	void jButton_imgsrcNext_actionPerformed(ActionEvent e) {
		jMenuItem_imgsrcNext_actionPerformed(e);
	}

	void jMenuItem_about_actionPerformed(ActionEvent e) {
		JOptionPane
				.showMessageDialog(
						this,
						"ImageMap v1.0\nCopyright (c) 2001-2003 Andreas Tetzl\nE-Mail: andreas@tetzl.de\nwww.tetzl.de\n\nThis program is Freeware.",
						"About", JOptionPane.PLAIN_MESSAGE);
	}

	void jButton_about_actionPerformed(ActionEvent e) {
		jMenuItem_about_actionPerformed(e);
	}

	void jButton_new_actionPerformed(ActionEvent e) {
		jMenuItem_new_actionPerformed(e);
	}

	void jMenuItem_exit_actionPerformed(ActionEvent e) {
		System.exit(0);
	}

	void jButton_help_actionPerformed(ActionEvent e) {
		jMenuItem_help_actionPerformed(e);
	}

	void jMenuItem_help_actionPerformed(ActionEvent e) {
		HelpFrame hf = new HelpFrame();
	}

	public void actionPerformed(ActionEvent e) { // popup menu
		JMenuItem source = (JMenuItem) (e.getSource());
		if (source == menuItem_remobject) {
			shapeList.removeFoundShape();
			imagemapPanel.repaint();
			mapChanged = true;
		} else if (source == menuItem_rempoint) {
			shapeList.getFoundShape().remove_polypoint(
					shapeList.getFoundKeyPoint());
			imagemapPanel.repaint();
			mapChanged = true;
		} else if (source == menuItem_convert) {
			ImagemapShape s = shapeList.getFoundShape();
			if (s.get_type() == ImagemapShape.TYPE_CIRCLE) {
				String str = JOptionPane.showInputDialog(this,
						"Enter number of circle segments in new polygon");
				int n = new Integer(str).intValue();
				s.convert(n);
			} else
				s.convert();
			imagemapPanel.repaint();
			mapChanged = true;
		} else if (source == menuItem_editInfo) { // forrest
			if (areaDlg == null) {
				areaDlg = new ImageAreaEdit();
				WindowListener listener = new WindowAdapter() {
					// public void windowOpened(WindowEvent e) { }

					public void windowClosed(WindowEvent e) {
						ImageAreaEdit dlg = (ImageAreaEdit) e.getSource();
						// System.out.println("JImageAreaEdit windowClosed.");
						if (dlg.okResult() && selectedShape != null) {
							selectedShape.set_name(dlg.getAreaName());
							selectedShape.set_comp(dlg.getComparitor());
						}
						selectedShape = null;
					}
				};
				areaDlg.addWindowListener(listener);
				areaDlg.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			}
			if (!areaDlg.isShowing() && selectedShape != null) {
				if (selectedShape.get_name() != null)
					areaDlg.setAreaName(selectedShape.get_name());
				if (selectedShape.get_comp() != null)
					areaDlg.setComparitor(selectedShape.get_comp());
				areaDlg.setVisible(true);
			}
		}
	}

	class PopupListener extends MouseAdapter {
		public void mousePressed(MouseEvent e) {
			maybeShowPopup(e);
		}

		public void mouseReleased(MouseEvent e) {
			maybeShowPopup(e);
		}

		private void maybeShowPopup(MouseEvent e) {
			int x = e.getX() / imagemapPanel.zoomFactor;
			int y = e.getY() / imagemapPanel.zoomFactor;
			if (e.isPopupTrigger()
					&& (shapeList.isPointInsideShape(x, y) || shapeList
							.isShapePointAt(x, y))) {
				menuItem_rempoint.setEnabled(shapeList.getFoundKeyPoint() > 0); // poly
																				// point
				menuItem_convert
						.setEnabled(shapeList.getFoundShape().get_type() == ImagemapShape.TYPE_CIRCLE
								|| shapeList.getFoundShape().get_type() == ImagemapShape.TYPE_RECT);
				selectedShape = shapeList.getFoundShape(); // forrest
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		}
	}

	public void setMapName(String str) {
		imageMap.setName(str);
	}

	void reportError(String str) {
		System.err.println(str);
		statusBar.setText(str);
	}

	public boolean loadViewImage(String imageSrc) {
		boolean rval = false;
		System.out.println("loadViewImage " + imageSrc);
		if (imageSrc.length() < 2)
			return rval;
		if (imageSrc.startsWith("http://")) {
			try {
				URL url = new URL(imageSrc);
				System.out
						.println("ImageIO.read(new URL('" + imageSrc + "'));");
				image = ImageIO.read(url);
				rval = true;
			} catch (Exception ex) {
				reportError("Error reading URL: " + imageSrc + ", "
						+ ex.getMessage());
			}
		} else {
			image = getToolkit().getImage(imageSrc);
			image_file = new File(imageSrc);
			MediaTracker tracker = new MediaTracker(this);
			tracker.addImage(image, 0);
			try {
				tracker.waitForID(0);
				if (tracker.isErrorAny()) {
					reportError("Error reading " + imageSrc
							+ ", MediaTracker error: "
							+ tracker.getErrorsAny().toString());
				} else
					rval = true;
			} catch (InterruptedException ie) {
				reportError("Error reading " + imageSrc
						+ ", InterruptedException: " + ie.getMessage());
			}
		}
		if (!rval)
			image = emptyImage;
		imagemapPanel.setImage(image);
		imagemapPanel.setImageFilename(image_file.getName());
		return rval;
	}

	void jScrollPane_image_componentShown(ComponentEvent e) {
		/* No Longer Support manual editing of the JSON code.
		 * ==================================================
		// When tabbing from JSON text view to Image view...
		if (jTextArea_json.getText().length() < 10)
			return;
		try {
			// Someone may have manually changed the ImageMap text
			ImageMap imagemap = getImageMapFromJSON(jTextArea_json.getText());
			createShapeList(shapeList, imagemap);
			// shapeList.set_mapName(imagemap.getName());

			if (!imageMap.compare(imagemap)) {
				if (loadViewImage(imagemap.getSrc())) {
					imagemap.setDimension(new Dimension(image.getWidth(null),
							image.getHeight(null)));
					// imagemapPanel.setImage(image);
					jMenuItem_zoom1.setSelected(true);
					imagemapPanel.repaint();
					imageMap = imagemap;
				}
			}
			statusBar.setText(" ");
		} catch (Exception ex) {
			reportError("Error parsing TextArea ImageMap: " + ex.getMessage());
		}
		*/
	}

	void jPanel_json_componentShown(ComponentEvent e) {
		// When tabbing from Image view to JSON text view...
		jTextArea_json.setText(shapeList.get_json(imageMap));
		statusBar.setText(" ");
	}

	public ImageMap getImageMapFromJSON(String json) throws Exception {
		Gson gson = new Gson();
		ImageMap imagemap = gson.fromJson(json, ImageMap.class);
		return imagemap;
	}

	public void addShapeList(ShapeList sl, Hashtable<String, ImageArea> areas)
			throws Exception {

		if (areas != null && areas.size() > 0) {
			ImagemapShape imshape = null;
			ImageArea area = null;
			Shape shape = null;
			for (String key : areas.keySet()) {
				imshape = null;
				area = areas.get(key);
				shape = area.getShape();
				System.out.println(key + " = " + areas.get(key));
				System.out.println(shape);

				if (shape.getType().compareToIgnoreCase("rect") == 0) {
					Rectangle rect = shape.getRect();
					imshape = new ImagemapShape(rect.x, rect.y, rect.x
							+ rect.width, rect.y + rect.height);
				} else if (shape.getType().compareToIgnoreCase("poly") == 0) {
					Polygon poly = shape.getPoly();
					imshape = new ImagemapShape(poly);
				} else if (shape.getType().compareToIgnoreCase("circle") == 0) {
					Circle circle = shape.getCircle();
					imshape = new ImagemapShape(circle);
				} else
					throw new Exception("illegal shape " + shape.getType());
				imshape.setId(area.getId());
				imshape.set_comp(area.getComparitor());
				imshape.set_name(area.getName());
				imshape.setImageFile(area.getImageFile());
				sl.add_shape(imshape);
			}
		}
	}

	public void createShapeList(ShapeList sl, ImageMap imagemap)
			throws Exception {
		sl.clear();
		Hashtable<String, ImageMap> maps = imagemap.getImagemaps();

		addShapeList(sl, imagemap.getAreas());
		// sl.set_mapName(imagemap.getName());

		if (maps != null && maps.size() > 0) {
			for (ImageMap map : maps.values()) {
				System.out.println("ImageMap: " + map.getName());
				addShapeList(sl, map.getAreas());
			}
		}
	}

	private static String readUrl(String urlstr) throws Exception {
		BufferedReader reader = null;
		try {
			URL url = new URL(urlstr);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
	 
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}
			reader = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));
			StringBuffer buffer = new StringBuffer();
			int read;
			char[] chars = new char[1024];
			while ((read = reader.read(chars)) != -1)
				buffer.append(chars, 0, read);

			return buffer.toString();
		} finally {
			if (reader != null)
				reader.close();
		}
	}

	String readJsonFile(File file) {
		String rval = null;
		try {
			String propname = file.getCanonicalPath();
			int lastDot = propname.lastIndexOf('.');
			if (lastDot >= 0)
				propname = propname.substring(0, lastDot) + ".json";
			else
				propname += ".json";

			rval = new String(Files.readAllBytes(Paths.get(propname)));
		} catch (Exception ex) {
			reportError("Error could not open file: " + ex.getMessage());
		}
		return rval;
	}

	void parseJsonImageMap(String json) {
		String imageSrc = null;
		try {
			ImageMap imagemap = getImageMapFromJSON(json);
			createShapeList(shapeList, imagemap);
			if (shapeList.size() == 0)
				statusBar.setText("no imagemap found");
			/*
			if (imagemap.getImagesource_id() > -1) {
				imageSrc = String.format("%simagesource/%d/", this.appURL,
						imagemap.getImagesource_id());
				String json_text = readUrl(imageSrc);
				Gson gson = new Gson();
				ImageSource imgsrc = gson
						.fromJson(json_text, ImageSource.class);
				imagemap.setSrc(imgsrc.getPath());
				imagemap.setImagesource_id(imgsrc.getId());
			} else */if (!(new File(getImagePath(imagemap.getSrc()))).exists()) {
				// supports absolute paths D:\mydir\myyadda.ext
				throw new Exception("ImageSource id and src not set");
			}

			if (loadViewImage(getImagePath(imagemap.getSrc()))) {
				imagemap.setDimension(new Dimension(image.getWidth(null), image
						.getHeight(null)));
				// imagemapPanel.setImage(image);
				jMenuItem_zoom1.setSelected(true);
				jTextArea_json.setText(shapeList.get_json(imageMap));
				imageMap = imagemap;
				mapChanged = false;
			}
		} catch (Exception ex) {
			reportError("Error parsing ImageMap: " + ex.getMessage());
		}
	}

	String getImagePath(String fname) {
		String rval = this.image_dir + File.separator + fname;
		return rval;
	}
	void parseJsonImageMapFile(File file) {
		String json = readJsonFile(file);
		parseJsonImageMap(json);
	}

	void parseJsonImageMapUrl(String url) throws Exception {
		String json = readUrl(url);
		parseJsonImageMap(json);
	}

	void select_imagemap_file_dialog() {
		File file = null;

		JFileChooser chooser = new JFileChooser(directory);
		chooser.setFileFilter(new ImageMapFilter());
		chooser.setDialogTitle("Open/Parse JSON ImageMap File");
		// chooser.setLocale(currentLocale);
		if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			File f = chooser.getSelectedFile();
			if (f.exists()) {
				file = f;
			} else {
				reportError("Error file does not exist");
				return;
			}
		} else
			return; // FileChooser Cancel

		parseJsonImageMapFile(file);
	}

	void select_imagemaps_dialog() {
		String imgmapsURL = String.format("%sdevicemaps/", this.appURL);
		String json_text;
		try {
			json_text = readUrl(imgmapsURL);
			Gson gson = new Gson();
			ImageMap[] sources = gson.fromJson(json_text, ImageMap[].class);
			HashMap<String, ImageMap> map = new HashMap<String, ImageMap>();
			for (int i = 0; i < sources.length; i++) {
				map.put(sources[i].getName(), sources[i]);
			}
			Object[] objs = map.keySet().toArray();
			String input = (String) JOptionPane.showInputDialog(this,
					"Select ImageMap", "Select ImageMap",
					JOptionPane.QUESTION_MESSAGE, null, // Use default icon
					objs, objs[0]);
			ImageMap selected = map.get(input);
			updateImageMap(selected);
		} catch (Exception e1) {
			reportError("Error readUrl('" + imgmapsURL + "'): "
					+ e1.getMessage());
		}
	}
	/*
	void select_imagesource_dialog() {
		String sourcesURL = String.format("%simagesources/", this.appURL);
		String json_text;
		try {
			json_text = readUrl(sourcesURL);
			Gson gson = new Gson();
			ImageSource[] sources = gson.fromJson(json_text,
					ImageSource[].class);
			HashMap<String, ImageSource> map = new HashMap<String, ImageSource>();
			for (int i = 0; i < sources.length; i++) {
				map.put(sources[i].getName(), sources[i]);
			}
			Object[] objs = map.keySet().toArray();
			String input = (String) JOptionPane.showInputDialog(this,
					"Select ImageSource", "Select ImageSource",
					JOptionPane.QUESTION_MESSAGE, null, // Use default icon
					objs, objs[0]);
			ImageSource selected = map.get(input);
			updateImageSource(selected);
		} catch (Exception e1) {
			reportError("Error readUrl('" + sourcesURL + "'): "
					+ e1.getMessage());
		}
	}
	*/
	void select_imagesource_file_dialog() {
		String fname = null;
		JFileChooser chooser = new JFileChooser(image_dir);
		chooser.setDialogTitle("Open Image File");
		if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			File f = chooser.getSelectedFile();
			if (f.exists()) {
				fname = f.toString();
				image_file = f;
				image_dir = f.getParent();
			} else
				statusBar.setText("file does not exist");
		}

		if (fname != null) {
			//String json;
			try {
				//json = new String(Files.readAllBytes(Paths.get(fname)));
				//Gson gson = new Gson();
				//ImageSource source = gson.fromJson(json, ImageSource.class);
				ImageSource source = new ImageSource(fname);
				updateImageSource(source);
			} catch (Exception e1) {
				System.err.println("Error getting " + fname + ": " + e1);
			}
		}
	}


	void select_imagesource(String direction) {

		if (this.image_file != null) {
			File file = null;
			File folder = image_file.getParentFile();
			File[] listOfFiles = folder.listFiles();
			HashMap<String, File> imageFiles = new HashMap<String, File>();
			for (int i = 0; i < listOfFiles.length; i++) {
				file = listOfFiles[i];
				if (file.isFile() && (
					file.getName().toLowerCase().endsWith(".jpg") ||
					file.getName().toLowerCase().endsWith(".png"))) {
					imageFiles.put(file.getName(), file);
				}
			}
			Object[] keys = imageFiles.keySet().toArray();
			// Sort the keys so the files are ordered
			Arrays.sort(keys);
			int index = Arrays.asList(keys).indexOf(image_file.getName());
			int next_index = (direction == "prev" ? index-1: index+1);
			if (next_index >= keys.length)
				next_index = 0;
			else if (next_index < 0)
				next_index = keys.length-1;
			String nextfile = null;
			try {
				image_file = imageFiles.get(keys[next_index]);
				nextfile = image_file.getCanonicalPath();
				updateImageSource(new ImageSource(nextfile));
				statusBar.setText("Opened " + image_file.getName());
				this.imageMap.setSrc(image_file.getName());
				//jTabbedPane1.setTitleAt(0, image_file.getName());
				// currentFile.setText(image_file.getName());
			}
			catch (Exception ex) {
				statusBar.setText("Exception getting "+direction+" image");
	        }
		}
		else
			statusBar.setText("no image file is currently open");
	}

	void jMenuItem_imgmapFile_actionPerformed(ActionEvent e) {
		select_imagemap_file_dialog();
	}

	void jButton_imgmapFile_actionPerformed(ActionEvent e) {
		select_imagemap_file_dialog();
	}
	/*
	void jButton_imgmapSelect_actionPerformed(ActionEvent e) {
		select_imagemaps_dialog();
	}

	void jButton_imgsrcSelect_actionPerformed(ActionEvent e) {
		select_imagesource_dialog();
	}
	*/
	boolean readImageFile(String imageSrc) {
		boolean rval = loadViewImage(imageSrc);
		if (!rval) {
			reportError("could not load image: " + imageSrc);
		}
		imageMap.setDimension(new Dimension(image.getWidth(null), image
				.getHeight(null)));
		imageMap.setSrc(imageSrc);
		// imagemapPanel.setImage(image);
		jTextArea_json.setText(shapeList.get_json(imageMap));
		return rval;
	}

	void updateImageSource(ImageSource imgsource) {
		System.out.println("updateImageSource(): " + imgsource.toString());
		readImageFile(imgsource.getPath());
		imageMap.setSrc(imgsource.getPath());
		imageMap.setImagesource_id(imgsource.getId());
	}

	void updateImageMap(ImageMap imagemap) throws Exception {
		System.out.println("updateImageMap(): " + imagemap.toString());
		parseJsonImageMapUrl(String.format("%sdevicemap/%d/", this.appURL,
				imagemap.getId()));
	}

	void jMenuItem_imgsrcFile_actionPerformed(ActionEvent e) {
		select_imagesource_file_dialog();
	}
	void jMenuItem_imgsrcPrev_actionPerformed(ActionEvent e) {
		select_imagesource("prev");
	}
	void jMenuItem_imgsrcNext_actionPerformed(ActionEvent e) {
		select_imagesource("next");
	}

	void resetImageMap() {
		image = emptyImage;
		imageMap = new ImageMap(-1, DEFAULT_MAP_NAME, null, new Dimension(
				image.getWidth(null), image.getHeight(null)));
		shapeList.clear();
		imagemapPanel.setImage(image);
		jTextArea_json.setText(shapeList.get_json(imageMap));
	}

	void jMenuItem_new_actionPerformed(ActionEvent e) {
		resetImageMap();
	}

	void jMenuItem_zoom_actionPerformed(ActionEvent e) {
		int zoomFactor = 0;
		if (e.getSource() == jMenuItem_zoom1)
			zoomFactor = 1;
		else if (e.getSource() == jMenuItem_zoom2)
			zoomFactor = 2;
		else if (e.getSource() == jMenuItem_zoom4)
			zoomFactor = 4;
		else if (e.getSource() == jMenuItem_zoom8)
			zoomFactor = 8;
		if (imagemapPanel.zoom(zoomFactor) == false)
			jMenuItem_zoom1.setSelected(true);
		vRule.repaint();
		hRule.repaint();
	}

	/** Overridden so we can exit when window is closed */
	protected void processWindowEvent(WindowEvent e) {
		super.processWindowEvent(e);
		if (e.getID() == WindowEvent.WINDOW_CLOSING) {
			System.exit(0);
		}
	}

	public void setAppURL(String url) {
		appURL = url;
	}

	/** Dialog to negotiate new ImageMap name with the server.
	 * We offer a name and server says Success or error_message that we display to user.
	 * Iterate until Cancel or until Success name is OK'd, returning name.
	 */
	String imagemap_name_dialog() {
		String mapnameURL = String.format("%sdevicemaps/", this.appURL);
		String rval = null;
		String json_text;
		try {
			Object[] objs = null; // map.keySet().toArray();
			rval = (String) JOptionPane.showInputDialog(this,
					"Select Name for New ImageMap", "Select Name",
					JOptionPane.QUESTION_MESSAGE, null, // Use default icon
					objs, null);
			System.out.println("imagemap.name="+rval);
			//json_text = readUrl(mapnameURL);
			//Gson gson = new Gson();
			//ImageMap[] sources = gson.fromJson(json_text, ImageMap[].class);
			
		} catch (Exception e1) {
			reportError("Error imagemap_name_dialog(...): "
						+ e1.getMessage());
		}
		return rval;
	}

	/**
	 * Post data to url and return any response in a string, else exception.
	 * @param url
	 * @param postdata
	 * @return If HttpResult is OK, return String containing any response data.
	 *		   any non-OK response throws and Exception.
	 */
	String post_to_URL(URL url, String data) throws Exception {
		String rval = null;
		System.out.println("post_to_URL: url=" + url + 
						   ",\n            data=" + data);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Accept", "application/json");
		con.setRequestMethod("POST");
		OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
		wr.write(data);
		wr.flush();
		// display return from the POST request
		StringBuilder sb = new StringBuilder();
		int HttpResult = con.getResponseCode();
		if (HttpResult == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					con.getInputStream(), "utf-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}
			br.close();
			rval = sb.toString();

		} else {
			throw new Exception(HttpResult + ": " + con.getResponseMessage());
		}
		System.out.println("Post OK: rval=" + rval);
		return rval;
	}

	void jButton_postjson_actionPerformed(ActionEvent e) {
		try {
			String imapName = imageMap.getName();
			String response = null;
			String postdata = null;
			boolean success = false;
			
			//while (!success) {
				if (imageMap.getId() < 0 || 
					imageMap.getName() == null || 
					imageMap.getName().equalsIgnoreCase(DEFAULT_MAP_NAME)) {
					imapName = imagemap_name_dialog();
					if (imapName != null && imapName.length() > 0) 
						imageMap.setName(imapName);
					else
						return;
				}
				
				postdata = shapeList.get_json(imageMap);
				saveToFile(postdata, jsonSaveDir + imapName + ".json"); // DEBUG
				try {
					// TODO: NEED TO CREATE DEVICEMAP FIRST! cause id=-1 no good
					String imapurl  = String.format("%sdevicemap/%d/", this.appURL, imageMap.getId());
					response = post_to_URL(new URL(imapurl), postdata);
					System.out.println("post_to_URL response="+response);
					success = true;
				} catch (Exception ex) {
					// TODO: Check for name-already-in-use
					reportError("Error Posting ImageMap: " + ex.getMessage());
				}
			//}
		}
		catch (Exception ex) {
			reportError("Error jButton_postjson_actionPerformed: " + 
						ex.getMessage());
		}
	}

	void saveToFile(String str, String fpath) throws FileNotFoundException, IOException {
		File propFile = null;

		propFile = new File(fpath);
		OutputStreamWriter out = new OutputStreamWriter(
				new FileOutputStream(propFile));
		out.write(str);
		out.close();
		System.out.println("Saved " + propFile.getCanonicalPath());
	}

	void jButton_savejson_actionPerformed(ActionEvent e) {
		File f = null;
		String propname = "";
		JFileChooser chooser = new JFileChooser(this.directory);
		chooser.setFileFilter(new ImageMapFilter());
		chooser.setDialogTitle("Save Map File");
		// chooser.setLocale(currentLocale);
		if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
			f = chooser.getSelectedFile();
		} else
			return; // FileChooser Cancel

		try {
			propname = f.getCanonicalPath();
			int lastDot = propname.lastIndexOf('.');
			if (lastDot >= 0)
				propname = propname.substring(0, lastDot) + ".json";
			else
				propname += ".json";
			saveToFile(shapeList.get_json(imageMap), propname);
		} catch (Exception ex) {
			reportError("Error saveToFile("+propname+"): exception: "+ex.getMessage());
		}
		mapChanged = false;
	}

	void jButton_copyjson_actionPerformed(ActionEvent e) {
		jTextArea_json.selectAll();
		jTextArea_json.copy();
		statusBar.setText("JSON code copied to clipboard");
	}

	public class ImageMapFilter extends FileFilter {
		public boolean accept(File f) {
			if (f.isDirectory())
				return true;
			String fname = f.getName();
			int lastSlash = fname.lastIndexOf(".");
			if (lastSlash < 0)
				return false;
			String extension = fname.substring(lastSlash + 1);
			if (extension != null) {
				if (extension.equalsIgnoreCase("json")) {
					return true;
				} else {
					return false;
				}
			}
			return false;
		}

		public String getDescription() {
			return "Just .json";
		}
	}

	/** Main method */
	public static void main(String[] args) {
		String tmpstr = null;
		Imagemapper mapper = new Imagemapper();
		try {
			for (int i = 0; i < args.length && args[i] != null; i++) {
				String tmpArg = args[i];
				if (tmpArg.endsWith(".json"))
					mapper.parseJsonImageMapFile(new File(tmpArg));
				else if (args[i].equalsIgnoreCase("--imagemap")
						&& args[i + 1].startsWith("http://")) {
					mapper.parseJsonImageMapUrl(args[++i]);
				} else if (args[i].equalsIgnoreCase("--server")
						&& args[i + 1].startsWith("http://")) {
					mapper.setAppURL(args[++i]);
				} else if (tmpArg.endsWith(".jpg") || tmpArg.endsWith(".jpeg"))
					mapper.readImageFile(tmpArg);
				else if ((new File(tmpArg)).isDirectory())
					mapper.directory = tmpArg;
					tmpstr = mapper.directory + File.separator + "images";
					if ((new File(tmpstr)).isDirectory())
						mapper.image_dir = tmpstr;
					File jfile = new File(mapper.directory+File.separator+"imagemap.json");
					if (jfile.isFile())
						mapper.parseJsonImageMapFile(jfile);
				else
					mapper.setMapName(tmpArg);
				break; // only one parm
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// EXIT_ON_CLOSE == 3
		mapper.setDefaultCloseOperation(3);

		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		mapper.setLocation((d.width - mapper.getSize().width) / 2,
				(d.height - mapper.getSize().height) / 2);
		mapper.setVisible(true);
	}

}
