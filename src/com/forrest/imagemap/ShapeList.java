package com.forrest.imagemap;

import java.awt.Dimension;
import java.awt.Point;
import java.util.*;
import com.google.gson.*;
/**
 * Title: ImageMap Description: client side imagemap creator
 * Copyright (c) 2001 Company: webdesign-pirna.de
 * @author Andreas Tetzl
 * @version 1.0
 */

public class ShapeList {

	private Vector shapes = null;
	private ImagemapShape foundShape = null;
	private int foundKeyPoint = 0;

	public ShapeList() {
		shapes = new Vector();
	}
	public void add_shape(ImagemapShape s) {
		shapes.add(s);
	}

	public ImagemapShape get_shape(int i) {
		return (ImagemapShape) shapes.get(i);
	}

	public void remove_shape(ImagemapShape s) {
		shapes.remove(s);
	}

	public int size() {
		return shapes.size();
	}

	public void clear() {
		shapes.clear();
	}

	public ImagemapShape getFoundShape() {
		return foundShape;
	}

	public int getFoundKeyPoint() {
		return foundKeyPoint;
	}

	public int getFoundShapeIndex() {
		return shapes.indexOf(foundShape);
	}

	/**
	 * Clear Shape object that was found on the last call of isShapePointAt ()
	 */
	public void removeFoundShape() {
		if (foundShape != null)
			shapes.remove(foundShape);
	}

	/**
	 * calls for each Shape object tryAddPoint () method on
	 */
	public void addPoint(int x, int y) {
		for (int i = 0; i < size(); i++) {
			ImagemapShape s = (ImagemapShape) shapes.get(i);
			if (s.tryAddPoint(x, y))
				return;
		}
	}

	public void removePointAt(int x, int y) {
		for (int i = 0; i < size(); i++) {
			ImagemapShape s = (ImagemapShape) shapes.get(i);
			if (s.removePointAt(x, y))
				return;
		}
	}

	public boolean isShapePointAt(int x, int y) {
		for (int i = 0; i < size(); i++) {
			foundKeyPoint = ((ImagemapShape) shapes.get(i)).isKeyPoint(x, y);
			if (foundKeyPoint != 0) {
				foundShape = (ImagemapShape) shapes.get(i);
				return true;
			}
		}
		return false;
	}

	public boolean isPointInsideShape(int x, int y) {
		boolean rval = false;
		int smallest = Integer.MAX_VALUE;
		ImagemapShape shape = null;
		int areasize = 0;
		for (int i = size() - 1; i >= 0; i--) {
			shape = (ImagemapShape)shapes.get(i);
			if (shape.inside(x, y)) {
				if ((areasize = shape.getAreaSize()) < smallest) {
					foundShape = (ImagemapShape) shapes.get(i);
					foundKeyPoint = 0;
					rval = true;
				}
			}
		}
		return rval;
	}

	/** Assemble a ImageMap then serialize it.
	 * This should probably be in ImageMap with same method likewise in
	 * ImageArea and this should serialize just the shapes.
	 * Its here and it works so let's leave it. 
	 * -forrest
	 */
	public String get_json(ImageMap imap) {
		String rval = null;
		ImageArea area = null;
		Shape shape = null;
		// remove all areas and reload them from ShapeList
		imap.clearAreas(); 
		// create ImageArea for each ImagemapShape and add to ImageMap
		for (int i = 0; i < size(); i++) {
			ImagemapShape s = (ImagemapShape) shapes.get(i);
			shape =  s.get_shape();
			
			area = new ImageArea(s.get_name(), shape, s.get_comp());
			area.setId(s.getId());
			area.setImageFile(s.getImageFile());
			imap.addArea(area);
		}
		// Configure GSON
	    final GsonBuilder gsonBuilder = new GsonBuilder();
	    gsonBuilder.registerTypeAdapter(ImageMap.class, new ImageMapSerialiser());
	    gsonBuilder.setPrettyPrinting();
	    final Gson gson = gsonBuilder.create();
		// Serialize the ImageMap
		rval = gson.toJson(imap);
		return rval;
	}
}
