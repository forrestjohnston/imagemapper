/**
 * 
 */
package com.forrest.imagemap;


/**
 * @author forrest
 *
 */
public class ImageArea implements java.io.Serializable {
	int id;
	String name;
	String comparitor;
	String imagefile;
	Shape shape;	// should be java.awt.{Rectangle, Polygon, geom.Ellipse2D.Double}

	public ImageArea() {
		this(null, null, null);
	}
	/**
	 * @param name
	 * @param shape
	 */
	public ImageArea(String name, Shape shape, String comp) {
		super();
		this.name = name;
		this.shape = shape;
		this.comparitor = comp;
		this.imagefile = "";
	}
	public ImageArea(String name, Shape shape) {
		this(name, shape, null);
	}
	public String getImageFile() {
		return imagefile;
	}

	public void setImageFile(String md) {
		this.imagefile = md;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return this.name;
	}
	public void setComparitor(String comparitor) {
		this.comparitor = comparitor;
	}
	public String getComparitor() {
		return this.comparitor;
	}
	/**
	 * @param shape the shape to set
	 */
	public Shape getShape() {
		return this.shape;
	}
	/**
	 * @param shape the shape to set
	 */
	public void setShape(Shape shape) {
		this.shape = shape;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ImageArea(" + (shape != null ? "shape=" + shape + ", " : "null")
				+ (name != null ? "name=" + name + ", " : "null") + ")";
	}
	
	
}
