package com.forrest.imagemap;

import java.awt.Composite;

import javax.swing.JDialog;
import javax.annotation.PostConstruct;
import java.awt.GridLayout;
import javax.swing.JPanel;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.border.TitledBorder;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.ComboBoxModel;
import javax.swing.JRadioButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.UIManager;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent; // Eclipse4/WindowBuilder/SWTDesigner
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

public class ImageAreaEdit extends JDialog {
	public final static String namesSerialFile = "imageAreaNames";
	JComboBox comboName = new JComboBox();
	JComboBox comboComp = new JComboBox();
	private JComboBox comboPath;
	boolean okResult = false;
	
	public ImageAreaEdit() {
		setBounds(100, 100, 400, 180);
		setTitle("Edit ImageArea");
		setModal(true);
		setAlwaysOnTop(true);
		getContentPane().setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel panel_main = new JPanel();
		getContentPane().add(panel_main);
		panel_main.setLayout(new GridLayout(3, 0, 0, 0));
		
		JPanel panel_AreaName = new JPanel();
		panel_AreaName.setBorder(new TitledBorder(null, "Name of Area", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_main.add(panel_AreaName);
		panel_AreaName.setLayout(new GridLayout(0, 1, 0, 0));
		
		comboName = new JComboBox();
		comboName.setEditable(true);
		panel_AreaName.add(comboName);
		
		JPanel panel_Path = new JPanel();
		panel_Path.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "UI Path", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_main.add(panel_Path);
		panel_Path.setLayout(new GridLayout(0, 1, 0, 0));
		
		comboPath = new JComboBox();
		comboPath.setModel(new DefaultComboBoxModel(new String[] {"None", "Brightness Comparitor", "Color Comparitor", "Image Comparitor"}));
		comboPath.setSelectedIndex(0);
		panel_Path.add(comboPath);
		
		JPanel panel_Buttons = new JPanel();
		panel_main.add(panel_Buttons);
		panel_Buttons.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_OkCancel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_OkCancel.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		panel_Buttons.add(panel_OkCancel, BorderLayout.SOUTH);
		
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okResult = false;
				ImageAreaEdit.this.setVisible(false);
				ImageAreaEdit.this.dispose();
			}
		});
		panel_OkCancel.add(cancelButton);
		
		JButton okButton = new JButton("OK");
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String selected = null;
				okResult = true;
				ImageAreaEdit.this.setVisible(false);
				ImageAreaEdit.this.dispose();
				ComboBoxModel comboNameModel = comboName.getModel();
				int index = comboName.getSelectedIndex();
				if (index == -1) {
					selected = (String)comboName.getSelectedItem();
					if (selected != null)
						comboName.insertItemAt(selected,0);
				}
				serialize(namesSerialFile);
			}
		});
		panel_OkCancel.add(okButton);
		getRootPane().setDefaultButton(okButton);

		this.loadSerialized(namesSerialFile);
	}
	
	// the WindowBuilder / SWTDesigner tooling uses methods to figure out
	// that the class is an Eclipse 4 part

	// one method must be annotated with @PostConstruct and 
	// must receive a least a Composiste

	@PostConstruct
	public void createControls(Composite parent) {

	}
	
    void serialize(String name)
    {
        try {
        	ComboBoxModel comboNameModel = comboName.getModel();
            String userDir = System.getProperty("user.home");
            String serializedDir = "serialized";
            File dirFile = new File(userDir, serializedDir);
            if (!dirFile.exists()) dirFile.mkdirs();
            File serFile = new File(dirFile, name + ".ser");
            System.out.println("serialize file: "+serFile.getAbsolutePath());
            FileOutputStream fs = new FileOutputStream(serFile);
            ObjectOutput out = new ObjectOutputStream(fs);

            out.writeObject(comboNameModel);
            out.close();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }
    
    boolean loadSerialized(String name)
    {
        boolean rval = false;
        
        try {
            String userDir = System.getProperty("user.home");
            String serializedDir = "serialized";
            File dirFile = new File(userDir, serializedDir);
            File serFile = new File(dirFile, name + ".ser");
            if (serFile.exists()) {
                FileInputStream f2 = new FileInputStream(serFile);
                ObjectInputStream in = new ObjectInputStream(f2);
                Object obj = in.readObject();
                if (obj instanceof javax.swing.ComboBoxModel)
                  comboName.setModel((javax.swing.ComboBoxModel) obj);
                in.close();
                comboName.setSelectedItem(null);
                rval = true;
            }
            else {
                System.out.println("file not found: "+serFile.getAbsolutePath());
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return rval;
    }

	public void setAreaName(String name) { 
		DefaultComboBoxModel model = (DefaultComboBoxModel)comboName.getModel();
		if(model.getIndexOf(name) == -1 ) {
			model.addElement(name);
		}
		comboName.setSelectedItem(name);
	}

	public void setComparitor(String comp) {
		DefaultComboBoxModel model = (DefaultComboBoxModel)comboPath.getModel();
		if(model.getIndexOf(comp) == -1 ) {
			model.addElement(comp);
		}
		comboPath.setSelectedItem(comp);
	}

	public String getAreaName() { 
		String rval = null;
		if (comboName.getSelectedItem() != null)
			rval = comboName.getSelectedItem().toString();
		return rval;
	}

	public String getComparitor() { 
		String rval = null;
		if (comboPath.getSelectedItem() != null)
			rval = comboPath.getSelectedItem().toString();
		return rval;
	}
	
	public boolean okResult() { return okResult; }
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			ImageAreaEdit dialog = new ImageAreaEdit();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
