package com.forrest.imagemap;

public class ImageSource {

	String path;
	String name;
	int id;

	public ImageSource(String path) {
		this.path = path;
	}
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "ImageSource (name='" + (name != null ? name : "null") + "', " +
							"path='" + (path != null ? path : "null")+ "')";
	}
}
