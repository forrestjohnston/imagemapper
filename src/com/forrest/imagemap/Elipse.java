package com.forrest.imagemap;

import java.awt.Point;
import java.awt.geom.Ellipse2D;

public class Elipse extends Ellipse2D.Double {

	/**
	 * @param x
	 * @param y
	 * @param height
	 * @param width
	 */
	public Elipse(int x, int y, int height, int width) {
		super((double)x, (double)y, (double)height, (double)width);
	}
	/* Circle definition */
	public Elipse(Point p, int radius) {
		super((double)p.x, (double)p.y, (double)radius, (double)radius);
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Elipse e = new Elipse(10, 20, 30, 40);
		System.out.println("getBounds()="+e.getBounds());
	}

}
