package com.forrest.imagemap;

import java.awt.*;
import java.awt.geom.*;

/**
 * Title:        ImageMap
 * Description:  client side imagemap creator
 * Copyright:    Copyright (c) 2001
 * Company:      webdesign-pirna.de
 * @author Andreas Tetzl
 * @version 1.0
 *
 */

public class ImagemapShape {

	public static int TYPE_RECT = 1;
	public static int TYPE_CIRCLE = 2;
	public static int TYPE_POLY = 3;
	public static String shapestr[] = {"", "Rectangle", "Circle", "Polygon" };

	public static int POINT_RECT_UPPERLEFT	= -1;
	public static int POINT_RECT_UPPERRIGHT = -2;
	public static int POINT_RECT_LOWERLEFT	= -3;
	public static int POINT_RECT_LOWERRIGHT = -4;
	public static int POINT_RECT_INSIDE		= -5;
	public static int POINT_CIRCLE_RADIUS	= -6;
	public static int POINT_CIRCLE_INSIDE	= -7;
	public static int POINT_POLY_INSIDE		= -8;

	private int id;
	private int type;
	private Rectangle rect;
	private Circle circle;
	private Point circle_center;
	private int circle_r;
	private Polygon poly;
	private String name = "<empty>";
	private String comp = null;
	private String imagefile = "";

	private Point mouseDragPosition;

	public ImagemapShape(int shape) {
		this.id 	 = -1;
		this.type    = shape;
		this.name    = new String(shapestr[shape]);
	}

	/**
	 * Constructor for rectangle objects
	 */
	public ImagemapShape(int x, int y, int x2, int y2) {
		this(TYPE_RECT);
		this.rect = new Rectangle();
		set_rect(x, y, x2, y2);
	}

	/**
	 * Constructor for circle objects
	 */
	public ImagemapShape(Point m, int r) {
		this(TYPE_CIRCLE);
		this.circle = new Circle(m, r);
		this.circle_center = m;			// legacy
		this.circle_r = r ;				// legacy
	}
	/**
	 * Constructor for circle objects
	 */
	public ImagemapShape(Circle c) {
		this(TYPE_CIRCLE);
		this.circle = c;
		this.circle_center = c.getCenterPoint();	// legacy
		this.circle_r = c.getRadius();				// legacy
	}

	/**
	 * Constructor for polygon objects
	 */
	public ImagemapShape(Point first) {
		this(TYPE_POLY);
		poly = new Polygon();
		poly.addPoint((int) first.getX(), (int) first.getY());
	}


	public ImagemapShape(Polygon p) {
		this(TYPE_POLY);
		poly = p;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String get_name() {
		return name;
	}

	public void set_name(String name) {
		if (name == null)
			return;
		this.name = new String(name);
	}

	public void set_comp(String name) {
		if (name == null)
			return;
		this.comp = new String(name);
	}

	public String get_comp() {
		return comp;
	}
	public String getImageFile() {
		return imagefile;
	}

	public void setImageFile(String md) {
		this.imagefile = md;
	}
	
	int getPolygonArea(int xpoints[], int ypoints[], int npoints) {
		int i,j;
		int area = 0;
		for (i=0;i < npoints;i++) {
			j = (i + 1) % npoints;
			area += xpoints[i] * ypoints[j];
			area -= ypoints[i] * xpoints[j];
		}
		area /= 2;
		return(area < 0 ? -area : area);
	}
	
  public int getAreaSize() {
	  int rval = 0;
	  if (type == TYPE_RECT) {
		  rval = rect.width * rect.height;
	  }
	  else if (type == TYPE_CIRCLE) {
		  rval = (int) ((double)circle_r * (double)circle_r * Math.PI);
	  }
	  else if (type == TYPE_POLY) {
		  rval = getPolygonArea(poly.xpoints, poly.ypoints, poly.npoints);
	
	  }
	  return rval;
  }
  
  /** Create a Shape  */
  public Shape get_shape() {
	  Shape rval = null;

	  if (type == TYPE_RECT) {
		  rval = new Shape( (int)(rect.x), 
				  			(int)(rect.y),
				  			(int)(rect.x+rect.width),
				  			(int)(rect.y+rect.height));
	  }
      else if (type == TYPE_CIRCLE) {
    	  rval = new Shape( new Point(circle_center.x,
    			  					  circle_center.y),
    			  			circle_r);
      }
	  else if (type == TYPE_POLY) {
		  rval = new Shape(new Polygon(poly.xpoints, 
				  					   poly.ypoints, 
				  					   poly.npoints));

      }
	  return rval;
  }

  public String get_typeString() {
	  return shapestr[this.type];
  }

  public void draw(Graphics2D g2, int zoomFactor) {
    float dash1[] = {4.0f};
    Stroke oldStroke=g2.getStroke();
    BasicStroke bs = new BasicStroke(1.0f, BasicStroke.CAP_BUTT,
                                     BasicStroke.JOIN_MITER, 3.0f, dash1, 0.0f);

    if (type == TYPE_RECT) {
      int x=(int)rect.getX()*zoomFactor, y=(int)rect.getY()*zoomFactor;
      int w=(int)rect.getWidth()*zoomFactor, h=(int)rect.getHeight()*zoomFactor;

      g2.setColor(Color.black);
      g2.drawRect(x, y, w, h);
      g2.setStroke(bs);
      g2.setColor(Color.white);
      g2.drawRect(x, y, w, h);

      g2.setStroke(oldStroke);
      g2.setColor(Color.black);
      g2.fillRect(x-2, y-2, 4, 4);
      g2.fillRect(x+w-2, y-2, 4, 4);
      g2.fillRect(x+w-2, y+h-2, 4, 4);
      g2.fillRect(x-2, y+h-2, 4, 4);
      g2.setColor(Color.white);
      g2.drawRect(x-2, y-2, 4, 4);
      g2.drawRect(x+w-2, y-2, 4, 4);
      g2.drawRect(x+w-2, y+h-2, 4, 4);
      g2.drawRect(x-2, y+h-2, 4, 4);
    } else if (type == TYPE_CIRCLE) {
      int x=(int)circle_center.getX()*zoomFactor;
      int y=(int)circle_center.getY()*zoomFactor;
      int r=circle_r*zoomFactor;

      g2.setColor(Color.black);
      g2.drawOval(x-r,y-r,r*2,r*2);
      g2.setStroke(bs);
      g2.setColor(Color.white);
      g2.drawOval(x-r,y-r,r*2,r*2);

      g2.setStroke(oldStroke);
      g2.setColor(Color.black);
      g2.fillRect(x+r-2,y-2, 4, 4);
      g2.setColor(Color.white);
      g2.drawRect(x+r-2,y-2, 4, 4);

    } else if (type == TYPE_POLY) {
      Polygon pg=poly;
      if (zoomFactor!=1) {
        pg=new Polygon();
        for (int i=0; i<poly.npoints; i++)
            pg.addPoint(poly.xpoints[i]*zoomFactor, poly.ypoints[i]*zoomFactor);
      }
      g2.setColor(Color.black);
      g2.drawPolygon(pg);
      g2.setStroke(bs);
      g2.setColor(Color.white);
      g2.drawPolygon(pg);

      g2.setStroke(oldStroke);
      g2.setColor(Color.black);
      for (int i=0; i<poly.npoints; i++)
        g2.fillRect(poly.xpoints[i]*zoomFactor-2, poly.ypoints[i]*zoomFactor-2, 4, 4);
      g2.setColor(Color.white);
      for (int i=0; i<poly.npoints; i++)
        g2.drawRect(poly.xpoints[i]*zoomFactor-2, poly.ypoints[i]*zoomFactor-2, 4, 4);
    }
  }

  public void set_rect(int x1, int y1, int x2, int y2) {
    int x,y,w,h;
    if (x2>x1) {
      x=x1; w=x2-x1;
    } else {
      x=x2; w=x1-x2;
    }

    if (y2>y1) {
      y=y1; h=y2-y1;
    } else {
      y=y2; h=y1-y2;
    }

    rect=new Rectangle(x,y,w,h);
  }

  public void set_rect_ul(int mx, int my) {
    int x=rect.x, w=rect.width, y=rect.y, h=rect.height;
    if (mx<rect.x + rect.width) {
      x=mx; w=rect.x + rect.width - mx;
    } else {
      x=rect.x + rect.width - 1; w=1;
    }
    if (my<rect.y + rect.height) {
      y=my; h=rect.y + rect.height - my;
    } else {
      y=rect.y + rect.height - 1; h=1;
    }

    rect.setLocation(x,y);
    rect.setSize(w,h);
  }
  public void set_rect_lr(int mx, int my) {
    int w=mx - rect.x;
    if (w<1) w=1;
    int h=my - rect.y;
    if (h<1) h=1;
    rect.setSize(w,h);
  }
  public void set_rect_ur(int mx, int my) {
    int y=rect.y, h=rect.height;
    int w=mx - rect.x;
    if (w<1) w=1;
    if (my<rect.y + rect.height) {
      y=my; h=rect.y + rect.height - my;
    } else {
      y=rect.y + rect.height - 1; h=1;
    }
    rect.setLocation(rect.x, y);
    rect.setSize(w,h);
  }
  public void set_rect_ll(int mx, int my) {
    int x=rect.x, w=rect.width;
    if (mx<rect.x + rect.width) {
      x=mx; w=rect.x + rect.width - mx;
    } else {
      x=rect.x + rect.width - 1; w=1;
    }
    int h=my - rect.y;
    if (h<1) h=1;
    rect.setLocation(x, rect.y);
    rect.setSize(w,h);
  }



  public void set_circle(Point m, int r) {
    circle_center=new Point(m);
    circle_r=r;
  }

  public void set_polypoint(int p, int x, int y) {
    poly.xpoints[p-1]=x;
    poly.ypoints[p-1]=y;
  }

  /**
   * adds a new point to the polygon and returns its position for moving it
   */
  public int add_polypoint(int x, int y) {
    poly.addPoint(x,y);
    return poly.npoints;
  }

  public void remove_polypoint(int p) {
    if (type!=TYPE_POLY || poly.npoints<=3) return;
    Polygon pnew = new Polygon();
    for (int i=0; i<poly.npoints; i++)
      if (i!=p-1)
        pnew.addPoint(poly.xpoints[i], poly.ypoints[i]);

    poly=pnew;
  }

  public void remove_lastPolyPoint() {
    if (type!=TYPE_POLY) return;
    remove_polypoint(poly.npoints);
  }


  public int get_type() {
    return type;
  }

  public int isKeyPoint(int mx, int my) {
    if (type == TYPE_RECT) {
      int x=(int)rect.getX(), y=(int)rect.getY();
      int w=(int)rect.getWidth(), h=(int)rect.getHeight();
      if (mx>=x-2 && mx<=x+2 && my>=y-2 && my<=y+2) return POINT_RECT_UPPERLEFT;
      if (mx>=x+w-2 && mx<=x+w+2 && my>=y-2 && my<=y+2) return POINT_RECT_UPPERRIGHT;
      if (mx>=x+w-2 && mx<=x+w+2 && my>=y+h-2 && my<=y+h+2) return POINT_RECT_LOWERRIGHT;
      if (mx>=x-2 && mx<=x+2 && my>=y+h-2 && my<=y+h+2) return POINT_RECT_LOWERLEFT;
      if (inside(mx,my)) {
        mouseDragPosition=new Point(mx-x, my-y);
        return POINT_RECT_INSIDE;
      }
    } else if (type == TYPE_CIRCLE) {
      int x=(int)circle_center.x;
      int y=(int)circle_center.y;
      int r=circle_r;

      if (mx>=x+r-2 && mx<=x+r+2 && my>=y-2 && my<=y+2) return POINT_CIRCLE_RADIUS;
      if (inside(mx,my)) {
        mouseDragPosition=new Point(mx-x, my-y);
        return POINT_CIRCLE_INSIDE;
      }
    } else if (type == TYPE_POLY) {
      for (int i=0; i<poly.npoints; i++) {
        if (mx>=poly.xpoints[i]-3 && mx<=poly.xpoints[i]+3 && my>=poly.ypoints[i]-3 && my<=poly.ypoints[i]+3)
          return i+1;
      }
      if (inside(mx,my)) {
        mouseDragPosition=new Point(mx-poly.xpoints[0], my-poly.ypoints[0]);
        return POINT_POLY_INSIDE;
      }
    }
    return 0;
  }


  public void moveKeyPoint(int p, int mx, int my) {
    if (p==POINT_RECT_UPPERLEFT)
      set_rect_ul(mx, my);
    else if(p==POINT_RECT_LOWERRIGHT)
      set_rect_lr(mx, my);
    else if (p==POINT_RECT_UPPERRIGHT)
      set_rect_ur(mx, my);
    else if (p==POINT_RECT_LOWERLEFT)
      set_rect_ll(mx, my);
    else if (p==POINT_RECT_INSIDE)
      set_rect(mx - mouseDragPosition.x, my - mouseDragPosition.y,
               mx - mouseDragPosition.x + rect.width, my - mouseDragPosition.y + rect.height);
    else if (p==POINT_CIRCLE_RADIUS) {
      if (mx>(int)circle_center.getX())
        set_circle(new Point(circle_center.x, circle_center.y), mx - circle_center.x);
    } else if (p==POINT_CIRCLE_INSIDE)
      set_circle(new Point(mx - mouseDragPosition.x, my - mouseDragPosition.y), circle_r);

    else if (p==POINT_POLY_INSIDE) {
      int dx=mx - mouseDragPosition.x - poly.xpoints[0];
      int dy=my - mouseDragPosition.y - poly.ypoints[0];
      for (int i=0; i<poly.npoints; i++)
        set_polypoint(i+1, poly.xpoints[i] + dx, poly.ypoints[i] + dy);
    } else if (p>0) // polygon point
      set_polypoint(p, mx, my);
  }


  public boolean inside(int x, int y) {
    if (type == TYPE_RECT) {
      if (rect.contains(x,y)) return true;
    } else if (type == TYPE_CIRCLE) {
      int mr=(int)Math.sqrt( ( x - circle_center.x ) * ( x - circle_center.x ) +
                             ( y - circle_center.y ) * ( y - circle_center.y ) );
      if (mr<circle_r) return true;
    } else if (type == TYPE_POLY) {
      if (poly.contains(x,y)) return true;
    }
    return false;
  }

  /**
   * convert rect shape to polgygon
   */
  public void convert() {
    if (type!=TYPE_RECT) return;
    poly=new Polygon();
    poly.addPoint(rect.x, rect.y);
    poly.addPoint(rect.x + rect.width, rect.y);
    poly.addPoint(rect.x + rect.width, rect.y + rect.height);
    poly.addPoint(rect.x, rect.y + rect.height);
    type=TYPE_POLY;
    rect=null;
  }

  /**
   * convert circle shape to polygon
   * @param n number of circle segments in new polygon
   */
  public void convert(int n) {
    if (type!=TYPE_CIRCLE) return;
    poly=new Polygon();
    for (int i=0; i<n; i++)
      poly.addPoint(circle_center.x + (int)(Math.cos(i*Math.PI*2/n) * circle_r),
                    circle_center.y + (int)(Math.sin(i*Math.PI*2/n) * circle_r));
    type=TYPE_POLY;
    circle_center=null;
  }


  public Point minDistance(int x, int y, ImagemapShape activeShape, int movingPoint) {
    if (type == TYPE_POLY) {
      Point p=new Point(10000, 10000);
      for (int i=0; i<poly.npoints; i++) {
        Point np=new Point(poly.xpoints[i], poly.ypoints[i]);
        if (np.distance((double)x, (double)y) < p.distance((double)x, (double)y))
          if (!(activeShape==this && i+1==movingPoint)) p=np;
      }
      return p;
    } else if (type == TYPE_RECT) {
      if (activeShape==this) return null;
      Point[] points = {rect.getLocation(),
                        new Point(rect.x+rect.width, rect.y),
                        new Point(rect.x+rect.width, rect.y+rect.height),
                        new Point(rect.x, rect.y+rect.height) };
      Point p=points[0];
      for (int i=1; i<4; i++)
        if (points[i].distance((double)x, (double)y) < p.distance((double)x, (double)y))
          p=points[i];
      return p;
    } else {  // type == TYPE_CIRCLE
      if (activeShape==this) return null;
      Point[] points = {circle_center,
                        new Point(circle_center.x+circle_r, circle_center.y),
                        new Point(circle_center.x-circle_r, circle_center.y),
                        new Point(circle_center.x, circle_center.y-circle_r),
                        new Point(circle_center.x, circle_center.y+circle_r) };
      Point p=points[0];
      for (int i=1; i<5; i++)
        if (points[i].distance((double)x, (double)y) < p.distance((double)x, (double)y))
          p=points[i];
      return p;
    }
  }

  public int minXDistance(int x) {
    if (type == TYPE_POLY) {
        int p=10000;
        for (int i=0; i<poly.npoints; i++) {
            int np=poly.xpoints[i];
            if (Math.abs(x-np) < Math.abs(x-p))
                p=np;
        }
        return p;
    } else if (type == TYPE_RECT) {
        int[] points = {rect.x, rect.x+rect.height};
        int p=10000;
        for (int i=0; i<points.length; i++)
            if (Math.abs(x-points[i]) < Math.abs(x-p))
                p=points[i];
        return p;
    } else {  // type == TYPE_CIRCLE
        int[] points = {circle_center.x, circle_center.x+circle_r, circle_center.x-circle_r};
        int p=10000;
        for (int i=0; i<points.length; i++)
            if (Math.abs(x-points[i]) < Math.abs(x-p))
                p=points[i];
        return p;
    }
  }

  public int minYDistance(int y) {
    if (type == TYPE_POLY) {
        int p=10000;
        for (int i=0; i<poly.npoints; i++) {
            int np=poly.ypoints[i];
            if (Math.abs(y-np) < Math.abs(y-p))
                p=np;
        }
        return p;
    } else if (type == TYPE_RECT) {
        int[] points = {rect.y, rect.y+rect.height};
        int p=10000;
        for (int i=0; i<points.length; i++)
            if (Math.abs(y-points[i]) < Math.abs(y-p))
                p=points[i];
        return p;
    } else {  // type == TYPE_CIRCLE
        int[] points = {circle_center.y, circle_center.y+circle_r, circle_center.y-circle_r};
        int p=10000;
        for (int i=0; i<points.length; i++)
            if (Math.abs(y-points[i]) < Math.abs(y-p))
                p=points[i];
        return p;
    }
  }

  /**
   * Punkt mit Position (x,y) in Polygon NACH Punkt n einfugen
   */
  public void insertPoint(int n, int x, int y) {
    if (type!=TYPE_POLY) return;
    Polygon pnew = new Polygon();
    for (int i=0; i<poly.npoints; i++) {
       pnew.addPoint(poly.xpoints[i], poly.ypoints[i]);
       if (i==n) pnew.addPoint(x,y);
    }
    poly=pnew;
  }

  /**
   * checken, ob Abstand von Maus zu einer Polygon-Linie kleiner als 5 Pixel.
   * Wenn ja, neuen Punkt an Mausposition einfugen
   */
  public boolean tryAddPoint(int x, int y) {
    if (type!=TYPE_POLY) return false;
    double minDist=Line2D.ptSegDist((double)poly.xpoints[poly.npoints-1], (double)poly.ypoints[poly.npoints-1],
                          (double)poly.xpoints[0], (double)poly.ypoints[0],
                          (double)x, (double)y);
    int minPoint=poly.npoints-1;
    for (int i=1; i<poly.npoints; i++) {
      double l=Line2D.ptSegDist((double)poly.xpoints[i-1], (double)poly.ypoints[i-1],
                                (double)poly.xpoints[i], (double)poly.ypoints[i],
                                (double)x, (double)y);
      if (l<minDist) {
        minDist=l;
        minPoint=i-1;
      }
    }

    if (minDist<10.0) {
      insertPoint(minPoint, x, y);
      return true;
    }

    return false;
  }


  public boolean removePointAt(int x, int y) {
    if (type!=TYPE_POLY) return false;

    for (int i=0; i<poly.npoints; i++) {
      if (x>=poly.xpoints[i]-2 && x<=poly.xpoints[i]+2 &&
          y>=poly.ypoints[i]-2 && y<=poly.ypoints[i]+2) {
            remove_polypoint(i+1);
            return true;
          }
    }

    return false;
  }


  public String getCoordString(int mouseX, int mouseY) {
    if (type == TYPE_POLY) {
        return "x="+mouseX+"  y="+mouseY;
    } else if (type == TYPE_RECT) {
        return "x="+rect.x+"  y="+rect.y+"  w="+rect.width+"  h="+rect.height;
    } else {  // type == TYPE_CIRCLE
        return "x="+circle_center.x+"  y="+circle_center.y+"  r="+circle_r;
    }
  }

}
