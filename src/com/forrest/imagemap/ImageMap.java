/**
 * 
 */
package com.forrest.imagemap;
import java.awt.Dimension;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author forrest
 *
 */
public class ImageMap { // implements java.io.Serializable {
	int id;
	String name;
	String src;
	int imagesource_id;
	// int height;
	// int width;
	Dimension dimension;
	Hashtable<String, ImageMap> imagemaps;
	Hashtable<String, ImageArea> areas;
	
	public ImageMap() {
		super();
	}
	/**
	 * @param name
	 * @param src
	 * @param dimension
	 */
	public ImageMap(int id, String name, String src, Dimension dimension) {
		super();
		this.id = id;
		this.name = name;
		this.src = src;
		this.dimension = dimension;
		this.areas = new Hashtable<String, ImageArea>();
		this.imagemaps = new Hashtable<String, ImageMap>();
	}
	/* Never mind... only necessary when user may have changed the JSON text
	 * leave it here in case we need to compare later...
	public boolean compareTo(ImageMap imap) {
		boolean areas_cmp = (imap.areas.size() == this.areas.size());
		// Compare the area.keys and area(s)
		Iterator<Entry<String, ImageArea>> it = imap.areas.entrySet().iterator();
		String entry_key = null;
		ImageArea imap_area  = null;
		ImageArea area  = null;
		while (it.hasNext()) {
		  Entry<String, ImageArea> entry = it.next();
		  ### PICKUP HERE ###
		  if ((entry_key= entry.getKey()) == null || 
			  (area = this.areas.get(entry_key)) == null) ||
			  (iarea = entry.getValue().compareTo(area_key)
		  // Remove entry if key is null or equals 0.
		  if (entry.getKey() == null || entry.getKey() == 0) {
		    it.remove();
		  }
		}
		return (areas_cmp &&
				imap != null &&
				imap.id == this.id &&
				imap.name == this.name &&
				(
					(imap.dimension == null && this.dimension == null) ||
					(imap.dimension.getWidth() == imap.dimension.getWidth() &&
					 imap.dimension.getHeight() == imap.dimension.getHeight())
				) 
				);
	}
	*/
	
	public Hashtable<String, ImageMap> getImagemaps() {
		return imagemaps;
	}
	public void setImagemaps(Hashtable<String, ImageMap> imageMaps) {
		this.imagemaps = imageMaps;
	}
	
	public int getImagesource_id() {
		return imagesource_id;
	}
	public void setImagesource_id(int imagesource_id) {
		this.imagesource_id = imagesource_id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public String getSrc() {
		return src;
	}
	/**
	 * @param src the src to set
	 */
	public void setSrc(String src) {
		this.src = src;
	}
	/**
	 * @return the dimension
	 */
	public Dimension getDimension() {
		return dimension;
	}
	/**
	 * @param dimension the dimension to set
	 */
	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
	}
	/**
	 * @return the areas
	 */
	public Hashtable<String, ImageArea> getAreas() {
		return areas;
	}
	/**
	 * @param areas the areas to set
	 */
	public void setAreas(Hashtable<String, ImageArea> areas) {
		this.areas = areas;
	}
	/**
	 * @param areas the areas to set
	 */
	public void addArea(ImageArea area) {
		assert(area.getName() != null);
		this.areas.put(area.getName(), area);
	}
	public void clearAreas() {
		this.areas.clear();
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ImageMap [" + (name != null ? "name=" + name + ", " : "")
				+ (src != null ? "src=" + src + ", " : "")
				+ (dimension != null ? "dimension=" + dimension + ", " : "")
				+ "]";
	}
}
