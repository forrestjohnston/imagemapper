package com.forrest.imagemap;
import java.awt.Dimension;
import java.lang.reflect.Type;

import com.forrest.imagemap.ImageMap;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class ImageMapSerialiser implements JsonSerializer<ImageMap> {
	
	public JsonElement serialize(ImageMap imagemap, Type typeOfSrc, 
			JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        Dimension dim = imagemap.getDimension();
        jsonObject.addProperty("id", imagemap.getId());
        jsonObject.addProperty("name", imagemap.getName());
        jsonObject.addProperty("src", imagemap.getSrc()); // used only by this Imagemapper
        // jsonObject.addProperty("imagesource_id", imagemap.getImagesource_id());
        jsonObject.addProperty("width", dim.width);
        jsonObject.addProperty("height", dim.height);

        /* ImageMap.areas = new Hashtable<String, ImageArea>(); */
        
        final JsonElement jsonAreas = context.serialize(imagemap.areas);
        jsonObject.add("areas", jsonAreas);
        return jsonObject;
    }
}
