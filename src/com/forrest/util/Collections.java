package com.forrest.util;

import java.lang.reflect.Array;

public class Collections {
	/** Compare two objects of same or different type */
	private static boolean equals(Object a, Object b) { 
	    return a == b || (a != null && a.equals(b)); 
	} 
	
	/** returns array index of myObj in list myArray or -1 if not found */
	public static int getArrayIndex(Object myObj, Object[] myArray) {
		int arraySize = Array.getLength(myArray);
		for (int i = 0; i < arraySize; i++) {
			if (equals(myArray[i], myObj)) return i;  
		}
		return (-1); // didn't find a match
	}
}
