ImageMapper (Prototype V1.0)
===========================

Image2Data ImageMapper

Java AWT/Swing Desktop application to create image maps of Image2Data objects.

Image from CameraView is displayed in the editor
and ImageAreas are defined by drawing boundaries enclosing objects in the view.
ImageAreas are circles, ovals, rectangles, polygons.

Those ImageAreas are associated with Devices by name using ImageAreaEditor.

This app will be ported to a web application...
